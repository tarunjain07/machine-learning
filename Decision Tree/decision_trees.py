from math import log
import numpy as np
import operator

def calculateShannonEnt(dataSet):
    numEntries = len(dataSet)
    labelCounts = {}
    for featVect in dataSet:
        #print(featVect, featVect[-1])
        currentLabel = featVect[-1]
        if currentLabel not in labelCounts.keys():
            labelCounts[currentLabel] = 0
        #print(labelCounts)
        labelCounts[currentLabel] += 1
            
    shannonEnt = 0.0
    for key in labelCounts:
        prob = float(labelCounts[key])/numEntries
        shannonEnt -= prob * log(prob, 2)
    return shannonEnt

# Gini:Gini(E)=1−∑p2
def giniImpurity(dataSet):
    numEntries = len(dataSet)
    labelCounts = {}
    for featVect in dataSet:
        #print(featVect, featVect[-1])
        currentLabel = featVect[-1]
        if currentLabel not in labelCounts.keys():
            labelCounts[currentLabel] = 0
        #print(labelCounts)
        labelCounts[currentLabel] += 1
        
    gini_impurity = 0
    for key in labelCounts:
        prob = float(labelCounts[key])/numEntries
        gini_impurity += np.power(prob, 2)
        
    return (1- gini_impurity)



def splitDataSet(dataSet, axis, value):
    retDataSet = []
    for featVect in dataSet:
        #print(featVect, featVect[axis],value)
        if featVect[axis] == value:
            reducedFeatVect = featVect[:axis]
            #print(reducedFeatVect)
            reducedFeatVect.extend(featVect[axis+1:])
            #print(reducedFeatVect)
            retDataSet.append(reducedFeatVect)
            #print(retDataSet)
    return retDataSet




def chooseBestFeatureToSplit(dataSet):
    numFeatures = len(dataSet[0]) - 1
    baseEntropy = calculateShannonEnt(dataSet)

    bestInfoGain = 0.0
    bestFeature = -1
    for i in range(numFeatures):
        featList = [example[i] for example in dataSet]
        uniqueVals = set(featList)
      
        newEntropy = 0.0
        for value in uniqueVals:
            subDataSet = splitDataSet(dataSet, i, value)
            prob = len(subDataSet)/ float(len(subDataSet))
            newEntropy += prob * calculateShannonEnt(subDataSet)
        infoGain = baseEntropy - newEntropy
    
        if(infoGain > bestInfoGain):
            bestInfoGain = infoGain
            bestFeature = i
    return bestFeature


def majorityCnt(classList):
    classCount = {}
    for vote in classList:
        if vote not in classCount.keys():
            classCount[vote] = 0
        classCount[vote] += 1
    sortedClassCount = sorted(classCount.items(), key = operator.itemgetter(1), reverse=True)
    return sortedClassCount[0][0]




labels = ['no surfacing', 'flippers']

dataSet = [[1, 1, 'yes'],
           [1, 1, 'yes'],
           [1, 0, 'no'],
           [0, 1, 'no'],
           [0, 1, 'no']]

def createTree(dataSet, labels_):
    
    labels = labels_.copy()
    classList = [example[-1] for example in dataSet]        
    if classList.count(classList[0]) == len(classList):
        return classList[0]    
    if len(dataSet[0]) == 1:
        return majorityCnt(classList)    
    bestFeat = chooseBestFeatureToSplit(dataSet)    
    bestFeatLabel = labels[bestFeat]    
    myTree = {bestFeatLabel: {}}    
    del(labels[bestFeat])    
    featValues = [example[bestFeat] for example in dataSet]    
    unqiueVals = set(featValues)    
    for value in unqiueVals:
        subLabels = labels[:]
        myTree[bestFeatLabel][value] = createTree(splitDataSet(dataSet, bestFeat, value), subLabels)
    return myTree
        

def classify(inputTree, featLabels, testVect):
    print(inputTree)
    firstStr = list(inputTree.keys())[0]
    print(firstStr)
    secondDict = inputTree[firstStr]
    featIndex = featLabels.index(firstStr)
    print("featIndex = ",featIndex)
    for key in secondDict.keys():
        print("v = " ,testVect, key)
        if testVect[featIndex] == key:
            print(secondDict)
            if type(secondDict[key]).__name__ == 'dict':
                classLabel = classify(secondDict[key], featLabels, testVect)
            else:
                classLabel = secondDict[key]
    return classLabel


        
print()
myTree = createTree(dataSet, labels)

myTree = {'flippers': {0: 'no', 1: {'no surfacing': {0: 'yes', 1: 'no'}}}}
myTree



classify(myTree, labels, [1,0])
classify(myTree, labels, [0,0])
classify(myTree, labels, [1,1])




chooseBestFeatureToSplit(dataSet)   






splitDataSet(dataSet, 0, 1)
splitDataSet(dataSet, 0, 0)
splitDataSet(dataSet, 1, 1)



dataSet

calculateShannonEnt(dataSet)
giniImpurity(dataSet)


dataSet[0][-1] = 'maybe'
dataSet

calculateShannonEnt(dataSet)
giniImpurity(dataSet)